#!/bin/sh

SPLASH_TAG="araikc/alko"
MYSQL_TAG="araikc/mysql_alko"
WEB_TAG="araikc/betchili"

LOCAL_DATA_DIR="/data"

MYSQL_R_CID=$(sudo docker ps| grep $MYSQL_TAG | awk '{print $1}')
SPLASH_R_CID=$(sudo docker ps| grep $SPLASH_TAG | awk '{print $1}')

MYSQL_CID=$(sudo docker ps -a| grep $MYSQL_TAG | awk '{print $1}')
SPLASH_CID=$(sudo docker ps -a| grep $SPLASH_TAG | awk '{print $1}')

RERUN=false

if [ "$MYSQL_R_CID" == "" ] || [ "$SPLASH_R_CID" == "" ]; then
	RERUN=true
fi

if [ "$RERUN" == "true" ]; then
	echo "Restarting containers..."
	if [ "$MYSQL_CID" != "" ] && [ "$SPLASH_CID" != "" ]; then
		sudo docker stop $MYSQL_CID
		sudo docker stop $SPLASH_CID
		sudo docker rm $MYSQL_CID
		sudo docker rm $SPLASH_CID
	fi
	sudo docker run -d -p 8050:8050 $SPLASH_TAG python -m splash.server --port=8050
	sudo docker run -d -p 3306:3306  -v $LOCAL_DATA_DIR/alko/mysql:/var/lib/mysql  $MYSQL_TAG
	sudo docker run -d -p 80:80 -v $LOCAL_DATA_DIR/alko/source:/var/www/betchili.com $WEB_TAG /usr/sbin/apache2ctl -D FOREGROUND
	sleep 31
fi 

MYSQL_R_CID=$(sudo docker ps| grep $MYSQL_TAG | awk '{print $1}')

MYSQL_IP=$(sudo docker inspect $MYSQL_R_CID | python -c 'import json,sys;obj=json.load(sys.stdin);print obj[0]["NetworkSettings"]["IPAddress"]')

sudo sed -i -e "s/\$db\['default'\]\['hostname'\] = '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*';/\$db\['default'\]\['hostname'\] = '$MYSQL_IP';/g" /data/alko/source/CodeIgniter_2.1.3/application/config/development/database.php

