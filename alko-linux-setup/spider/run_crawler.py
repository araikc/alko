import os
import re
import MySQLdb
from alko.adb import adb

def main():
    run_crawler(0)

def run_crawler(tty):
    print 'Running crawler...'
    if os.path.isfile('log.txt'):
        os.remove('log.txt')
    # run scrapy
    os.system("scrapy crawl alko > log.txt 2>&1")
    check_results(tty)

def check_results(tty):
    print 'Checking results...'
    logdata=''
    with open ("log.txt", "r") as myfile:
        logdata=myfile.read().replace('\n', '')
    m = re.match('.*(error processing|Connection was refused).*', logdata)
    if m:
	    if tty:
		print "Something was wrong: please invoke \'source run.sh\' from command line"
		exit(1)
            print "Error encountered: re-runnig again. Please wait."
            db = adb.Model()
            m_id = db.get_last_id('months')
            db.delete_prods_with_month_id(m_id)
            db.delete_month(m_id)
            run_crawler(1)
    m = re.match('.*Data for this months already get.*', logdata)
    if m:
            print "Data for this month already get"
            exit(1)
    print "Successfully run"

# main entry
main()

