# -*- coding: utf-8 -*-

# Scrapy settings for mgdcium project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documentgdc here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

import os

ENV='development'

BOT_NAME = 'alko'

SPIDER_MODULES = ['alko.spiders']
NEWSPIDER_MODULE = 'alko.spiders'

if ENV=='production':
  SPLASH_URL = 'http://127.0.0.1:8050'
elif ENV=='development':
  SPLASH_URL = 'http://127.0.0.1:8050'

DOWNLOADER_MIDDLEWARES = {
        'scrapyjs.SplashMiddleware': 725,
        }
#DUPEFILTER_CLASS = 'scrapyjs.SplashAwareDupeFilter'
HTTPCACHE_STORAGE = 'scrapyjs.SplashAwareFSCacheStorage'
ITEM_PIPELINES= {
    'alko.pipelines.ALKOPipeline':100
}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'mgdcium (+http://www.yourdomain.com)'
