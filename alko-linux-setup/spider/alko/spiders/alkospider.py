from scrapy.spiders import Spider
from scrapy.selector import Selector
from scrapy.http import Request
from alko.items import ALKOItem
import re
import os
import sys
import json
import MySQLdb
sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'adb'))

import adb

class ALKOSpider(Spider):
        name="alko"

        # Constructor
        def __init__(self, *args, **kw):
          super(ALKOSpider, self).__init__(*args, **kw)
          self.db = adb.Model()
          if self.check_month():
            self.start_urls = self.get_category_urls()
            self.updated_urls=[]
          else:
            print "Data for this months already get"

        # Forward to splash
        def start_requests(self):
          for url in self.start_urls:
            yield Request(url, self.parse_pages, meta={
              'splash' : {
                    'endpoint': 'render.html',
                    'args': {'wait' : 1}
                },
              'url': url
              })

        # Main parser
        def parse_pages(self, response):
            url = response.request.meta['url']
	    hxs = Selector(response)
            qty = hxs.xpath("//div[@class='result-info-ingress']/h1/text()").extract()
            qty = qty[0].split(' ')[0]
            pages = '&page=%s' % (str((int(qty) / 20) + 1))
            category = url
            url = url+pages
            yield Request(url, self.parse, meta={
              'splash' : {
                    'endpoint': 'render.html',
                    'args': {'wait' : 1}
                },
              'category': category
              })

        # Page parser
	def parse(self, response):
            category = response.request.meta['category']
	    hxs = Selector(response)
            rows = hxs.xpath("//a[@class='result-link']")
            items = []
            for row in rows:
              item = ALKOItem()
              det_link = row.xpath("./@href").extract()
              m = re.match('.*products\/(\d+)\/.*', det_link[0])
              pid = ''
              if m: pid = m.group(1)
              det_link = 'http://www.alko.fi/api/product/Availability?productId='+pid+'&cityId=all&language=en'
              yield Request(det_link, meta={'item': item}, callback=self.get_details)
              item['Name'] = row.xpath(".//h3[contains(@class, 'result-title')]/text()").extract()
              item['Price'] = row.xpath(".//span[@class='price-value']/text()").extract()
              cat = row.xpath(".//div[contains(@class, 'taste-description')]/text()").extract()
              #item['Category'] = "%s" % (cat[0].strip())
              item['Category'] = category
              cc = row.xpath(".//span[@class='region']/text()").extract()
              item['Country'] = cc[0].split(',')[0]
              items.append(item)

        # Details parser
        def get_details(self, response):
          mitem = response.request.meta['item']
          data = json.loads(response.body_as_unicode())
          stores_qty = len(data)
          amount = sum([int(d['Amount']) for d in data])
          mitem['StoresCount'] = stores_qty
          mitem['AllItemsCount'] = amount
          yield mitem


        def get_category_urls(self):
          categories = self.db.get_categories()
          urls = []
          for c in categories:
            urls.append(c['url'])
          return urls

        def check_month(self):
            month = self.db.get_current_month()
            months = self.db.get_months()
            stat = True
            for m in months:
              if m['name'] == month:
                stat = False
            if stat:
              self.db.add_month(month)
              self.month_id=self.db.get_last_id('months')
            return stat

