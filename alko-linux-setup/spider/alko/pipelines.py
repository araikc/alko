# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import MySQLdb
from scrapy.exceptions import DropItem
from adb import adb

class ALKOPipeline(object):

    def __init__(self):
        self.db = adb.Model()
        self.month_id = self.db.get_last_id('months')
        self.categories_list = self.db.get_categories()

    def close_spider(self, spider):
        self.db.close()

    def process_item(self, item, spider):
        for ic in self.categories_list:
          if ic['url'] == item['Category']:
            category_id=ic['id']
        data={}
        data['name'] = item['Name'][0]
        data['category_id'] = category_id
        data['country'] = item['Country']
        data['price'] = item['Price'][0]
        data['stores_count'] = item['StoresCount']
        data['all_items_count'] = item['AllItemsCount']
        data['month_id'] = self.month_id
        self.db.add_product(data)

        return item

