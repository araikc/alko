
function UIUploader()
{
	this.myuploader;
	this.uploadId;
	this.reader; 
	this.uploadState;
	this.nophoto;
	
	this.uploadurl;
	this.uploadpath;
	this.basedeleteurl;
	this.deleteurl;
	
	this.cancelUpload = function()
	{
		this.myuploader.damnUploader("cancelAll");
		$("#ql_select_file").attr("value" , "");
		$("#ql_progress_bar").css("width" , "0%");
		$("#ql_image_prev").attr("src", this.nophoto);
		this.uploadState = "canceled";
		
	};
	
	this.createPreview = function (file)
	{
		this.reader.readAsDataURL(file);
	};
	
	this.uitemplate =  '<div class="row-fluid" style="margin-bottom:10px;" id="ql_dropbox">' +
		'<div class="span2" >' + 
			'<img id="ql_image_prev"  width="128px" height="128px">' + 
		'</div>' +	

		'<div class="span10" >' + 
			'<div id = "ql_progress_container" class="progress progress-striped row-fluid ">' +
				'<div id= "ql_progress_bar" class="bar" style="width: 0%;"></div>' +
			'</div>' +
			'<div class = "row-fluid">' +
				'<div class="span3">' +
					'<span id="ql_add_button" class="btn btn-success fileinput-button btn-small " >' +
						'<i class="icon-plus icon-white"></i>' +
						'<span>Select</span>'+
						'<input type="file" name="file"  id="ql_select_file">' +
					'</span>' +
				'</div>' +
				'<div class="span3">' +
					'<button id="ql_start_button" type="submit" class="btn btn-primary start btn-small ">' +
						'<i class="icon-upload icon-white"></i>' +
						'<span>Start</span>' +
					'</button>' +
				'</div>'+
				'<div class="span3">' +
					'<button id="ql_cancel_button" type="reset" class="btn btn-warning cancel btn-small ">' +
						'<i class="icon-ban-circle icon-white"></i>' +
						'<span>Cancel</span>' +
					'</button>' +
				'</div>' +
					
				'<div class="span3">' +
					'<button id="ql_delete_button"  type="button" class="btn btn-danger delete btn-small ">' +
						'<i class="icon-trash icon-white"></i>' + 
						'<span>Delete</span>' +
					'</button>' +
				'</div>	' +
			'</div>' +
		'</div>' +
	'</div>' ;
	
}

UIUploader.prototype.reset = function()
{
	this.cancelUpload();
};

UIUploader.prototype.setImage = function(image)
{
	$("#ql_image_prev").attr("src", this.uploadpath + image);
};

UIUploader.prototype.setUploadUrl = function(urlval)
{
	this.myuploader.damnUploader('setParam' , {url : urlval});
};

UIUploader.prototype.setDeleteUrl = function(urlval)
{
	this.deleteurl = urlval;
};

UIUploader.prototype.init = function(params, id)
{
	var self = this;

	$("#"+id).append(String(self.uitemplate));		
	
	self.uploadurl = params.uploadurl;
	self.uploadpath = params.uploadpath;
	self.nophoto = params.nophoto;
	self.deleteurl = params.deleteurl;
	self.basedeleteurl = params.deleteurl;
	
	self.reader = new FileReader();
	self.reader.onload = function(e)
	{
		$("#ql_image_prev").attr("src", e.target.result );
	};
	
	$("#ql_image_prev").attr("src", self.nophoto );
	
	self.myuploader = $("#ql_select_file").damnUploader({
		multiple  :false,
		url: self.uploadurl,
		dropping :true , 
		dropBox: $("#ql_dropbox"),
		onAllComplete :function() {
        
		},
	});
	
	
	self.myuploader.damnUploader('setParam', {'onSelect' : 
		function(file) {
				self.cancelUpload();
				self.createPreview(file);	
				
				self.uploadState = "selected";
				self.uploadId = this.damnUploader('addItem', {
					file: file,
					onProgress: function(percents) { var total = $("#ql_progress_container").width(); var progress = (percents * total / 100) ; $("#ql_progress_bar").css("width" , progress); },
					onComplete: function(successfully, data, errorCode) {
					  if (successfully) {
							
					  } else {
						
					  }
				  }
			  });				
			return false; 
    	}
	});
	
	$("#ql_start_button").on("click",function(){ if(self.uploadState == "selected") {self.uploadState  = "uploading";  self.myuploader.damnUploader('startUpload');} });
	$("#ql_cancel_button").on("click",function(){ self.cancelUpload(); });
	
	$("#ql_delete_button").on("click",function(){ 
		$.ajax({
		  type: 'POST',
		  url: self.deleteurl,
		  data: {},
		  success: function(res){
			  $(self).trigger("deleted");
		  }
		}); 
	});
	
};
