
// Utility directives //
qaApp.directive('qaEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.qaEnter);
                });
                event.preventDefault();
            }
        });
    };
});

qaApp.directive('qachosen', function() {
  return {
      restrict: "A",
      link: function(scope,element,attrs){
            var length = attrs.ngwidth;
            scope.$watch(attrs.qachosen, function(){
              element.trigger("chosen:updated");
            },true);
            scope.$watch(attrs.qachosenselected, function(){
              element.trigger("chosen:updated");
            },true);
        element.chosen({allow_single_deselect : true, width : length});
      }
  };
});

qaApp.directive('qadatetimepicker', function($rootScope) {
  return {
      restrict: "E",
      scope: {
        name:          "=",
        qadate:        "=",
        mindate:       "=",
        maxdate:       "=",
        showtime:      "=",
        showdate:      "=",
        qadatechange: "@",
        qashow:       "@",
        qalimits:     "@",
        qahide:       "@",
        format:        "@",
        weekstart:     "@"
      },
      link: function (scope, element, attrs){
            scope.secondsCoeff = 1;

            var weekstart = scope.weekstart ? parseInt(scope.weekstart) : 0;

            if(attrs.format == "seconds")
            {
              scope.secondsCoeff = 1000;
            }

            var pickDate,pickTime,format;
            if(scope.showdate && !scope.showtime)
            {
              pickDate = true;
              pickTime = false;
              format   = "yyyy-MM-dd";
            }
            else if(!scope.showdate && scope.showtime)
            {
              pickDate = false;
              pickTime = true;
              format   = "hh:mm";
            }
            else
            {
              pickDate = true;
              pickTime = true;
              format   = 'yyyy-MM-dd hh:mm';
            }
            scope.pickerElem = element.find(".qapicker");
            scope.inputElem  = scope.pickerElem.find("input");
            scope.pickerElem.datetimepicker({
              language: 'en',
              format: format,
              pickDate: pickDate,
              pickTime: pickTime,
              pick12HourFormat: false,
              weekStart : weekstart
            });

            scope.pickerAPI = scope.pickerElem.data('datetimepicker');
            
            scope.$watch("qadate", function(){
              if(scope.qadate) {
                scope.pickerAPI.setDate(new Date(parseInt(scope.qadate)* scope.secondsCoeff));  
              } else {
                scope.pickerAPI.setDate(null);
              }
            });

            scope.$watch("mindate", function(){ 
               scope.pickerAPI.setStartDate(new Date(scope.mindate * scope.secondsCoeff));
            });

            scope.$watch("maxdate", function(){ 
               scope.pickerAPI.setEndDate(new Date(scope.maxdate * scope.secondsCoeff));
            });

            scope.$on(attrs.qashow, function(){
               scope.pickerAPI.show();
            });

            scope.$on(attrs.qahide, function(e, arg){
               //reset gets reset data in miliseconds
               if(arg && arg.reset)
               {
                  scope.pickerAPI.setLocalDate(new Date(arg.reset * scope.secondsCoeff));
               }
              scope.pickerAPI.hide();
            });
            
            scope.pickerElem.on('changeDate', function(e) {
              $rootScope.$apply(function(){
                  var val = null;
                  if (e.localDate) {
                    val = (e.localDate.getTime() - e.localDate.getTimezoneOffset()*60000)/scope.secondsCoeff;
                    val = Math.round(val);
                    //val = e.localDate.getTime()/scope.secondsCoeff;
                  }
                  scope.$emit(scope.qadatechange, val);
              });

              if(scope.showdate && !scope.showtime)
              {
                scope.pickerAPI.hide();
              }
            });

            scope.pickerElem.on('hide', function(e){
              scope.$emit("dateTimePickerClose", scope.name);
            });
       },
        template :  '<span><span class="input-append datetimepicker2 qapicker date-class">'+
                      '<input  type="text" class="add-on"></input>'+
                      '<span class="add-on">'+
                        '<i class="glyphicon glyphicon-calendar icon-time-class">'+
                        '</i>'+
                      '</span>'+
                    '</span>'+
                    '</span>'
  };
});
