
qaApp.factory("MonthsAPI", function (QAConfigs, $http) {
	return {
		getProdCountsByMonth : function(m1, m2) {
						var ob = new Object();
						ob.m1 = m1;
						ob.m2 = m2;
						return $http({
			    			method: "POST",
			    			url: QAConfigs.BASE_INDEX + "qa/months/get_cat_prod_count_by_month",
			    			data :   $.param(ob),
			    			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});		
					},
		getStoresCountsByMonth : function(c_id, m1, m2) {
				var ob = new Object();
				ob.cat = c_id;
				ob.m1 = m1;
				ob.m2 = m2;
				return $http({
	    			method: "POST",
	    			url: QAConfigs.BASE_INDEX + "qa/months/get_prod_stores_count_by_month",
	    			data :   $.param(ob),
	    			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});		
			}
	};
});


function CompModel(scope) {
	this.month_id1 = null;
	this.month_id2 = null;
	this.firstName = null;
	this.secondName = null;

	this.prodCount = [{id : 0, name : 'All'}, {id : 1, name : 'Increased'}, 
						{id :2, name : 'Decreased'}];
	this.selCondition = 0;

	this.categoryList = [];
	this.selCatId = null;
	this.storeCount = [{id : 0, name : 'All'}, {id : 1, name : 'Gainers'}, 
						{id :2, name : 'Loosers'}];

	this.selInfo = 0;
	this.ready = false;

	return this;
}


function MonthsCtrl($scope, $http, $window, $timeout, QAConfigs, MonthsAPI, QAUtils ) {

	$scope.compModel = CompModel($scope);

	$scope.diffTypes1 = [{type: 0, value: "Biggest"}, {type: 1, value: "Smallest"}];
	$scope.diffType1 = 0;

	$scope.diffTypes2 = [{type: 0, value: "Biggest"}, {type: 1, value: "Smallest"}];
	$scope.diffType2 = 0;

	$scope.monthList = [];
	$scope.prodsByCat = null;
	$scope.prodsByCatSort = null;
	$scope.storesByMonth = null;
	$scope.storesByMonthSort = null;


	$scope.isShow = function(item) {
		if ($scope.compModel.selCondition == 1 && item.qty < item.qty2) {
			return true;
		} else if ($scope.compModel.selCondition == 2 && item.qty > item.qty2) {
			return true;
		} else if ($scope.compModel.selCondition == 0) {
			return true;
		}
		return false;
	};

	$scope.isGainLose = function(item) {
		if ($scope.compModel.selInfo == 1 && item.qty < item.qty2) {
			return true;
		} else if ($scope.compModel.selInfo == 2 && item.qty > item.qty2) {
			return true;
		} else if ($scope.compModel.selInfo == 0) {
			return true;
		}
		return false;
	};

	$scope.selectItem = function(id, name, $event) {
		if (id == $scope.compModel.month_id1) {
			$scope.compModel.month_id1 = null;
			$event.target.getElementsByTagName('span')[0].innerText = '';
			$scope.compModel.firstName = null;
			$scope.compModel.ready=false;
		} else if (id == $scope.compModel.month_id2) {
			$scope.compModel.month_id2 = null;
			$event.target.getElementsByTagName('span')[0].innerText = '';
			$scope.compModel.secondName = null;
			$scope.compModel.ready=false;
		} else {
			if ($scope.compModel.month_id1 == null) {
				$scope.compModel.month_id1 = id;
				$event.target.getElementsByTagName('span')[0].innerText = "1";
				$scope.compModel.firstName = name;
			} else if ($scope.compModel.month_id1 != null && $scope.compModel.month_id2 == null) {
				$scope.compModel.month_id2 = id;
				$event.target.getElementsByTagName('span')[0].innerText = '2';
				$scope.compModel.secondName = name;
			}
		}
	};

	$scope.showDiff = function() {
			if ($scope.compModel.month_id1 && $scope.compModel.month_id2) {
				$scope.compModel.ready = true;
			}
			QAUtils.block();
			MonthsAPI.getProdCountsByMonth($scope.compModel.month_id1, $scope.compModel.month_id2).success(function(data, status, headers, config) {
				if(data) {
					$scope.prodsByCat = data;
					$scope.sortByDifference1($scope.diffType1);
					$scope.storesByMonth = null;
					$scope.compModel.selCatId = null;
				}
		  		QAUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::prods showDiff");
	        	QAUtils.unblock();
	    	});
	};

	$scope.selectCategory = function() {
		QAUtils.block();
		MonthsAPI.getStoresCountsByMonth($scope.compModel.selCatId, $scope.compModel.month_id1, $scope.compModel.month_id2).success(function(data, status, headers, config) {
			if(data) {
				$scope.storesByMonth = data;
				$scope.sortByDifference2($scope.diffType2);
			}
	  		QAUtils.unblock();
  		}).error(function(data, status, headers, config) {
        	console.log("js::prods selectCategory");
        	QAUtils.unblock();
    	});
	};

	$scope.sort2 = function() {
		$scope.sortByDifference2($scope.diffType2);
	}

	$scope.sort1 = function() {
		$scope.sortByDifference1($scope.diffType1);
	}

	$scope.sortByDifference1 = function(pred) {
		$scope.prodsByCatSort = [];
		for (var a in $scope.prodsByCat) {
			$scope.prodsByCatSort.push({data: $scope.prodsByCat[a], value: Math.abs($scope.prodsByCat[a].qty-$scope.prodsByCat[a].qty2)});
		}
		if (!pred) {
			$scope.prodsByCatSort.sort(function(a, b) {return b.value - a.value});	
		} else {
			$scope.prodsByCatSort.sort(function(a, b) {return a.value - b.value});
		}
	};

	$scope.sortByDifference2 = function(pred) {
		$scope.storesByMonthSort = [];
		for (var a in $scope.storesByMonth) {
			$scope.storesByMonthSort.push({data: $scope.storesByMonth[a], value: Math.abs($scope.storesByMonth[a].qty-$scope.storesByMonth[a].qty2)});
		}
		if (!pred) {
			$scope.storesByMonthSort.sort(function(a, b) {return b.value - a.value});	
		} else {
			$scope.storesByMonthSort.sort(function(a, b) {return a.value - b.value});
		}
	};

	$scope.init = function() {
		$scope.monthList = [];
		$scope.compModel.categoryList = [];
		angular.forEach($window.categoryData, function(item, ind){
			$scope.compModel.categoryList.push(item);
		});
		angular.forEach($window.monthData, function(item, ind){
			$scope.monthList.push(item);
		});
		$scope.selected_month_id = $window.lastMonthId;
	};
}

