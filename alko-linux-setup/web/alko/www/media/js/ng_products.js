
qaApp.factory("ProductsAPI", function (QAConfigs, $http) {
	return {
		showProdsByFilter : function(filter) {
						var ob = new Object();
						ob.filter = filter;

						return $http({
			    			method: "POST",
			    			url: QAConfigs.BASE_INDEX + "qa/products/get_products_by_filter",
			    			data :  $.param(ob),
			    			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});		
					},
		showThreeProds : function(dir, m_id, count) {
						var ob = new Object();
						ob.dir = dir;
						ob.month_id = m_id;
						ob.count = count;

						return $http({
			    			method: "POST",
			    			url: QAConfigs.BASE_INDEX + "qa/products/get_prods_by_dir",
			    			data :  $.param(ob),
			    			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});	
		},
		getAllProdsCountByMonthId: function(m_id) {
						var ob = new Object();
						ob.m_id = m_id;

						return $http({
			    			method: "POST",
			    			url: QAConfigs.BASE_INDEX + "qa/products/get_prods_count_by_month_id",
			    			data :  $.param(ob),
			    			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						});	
		}
	};
});


function FilterModel(scope) {
	this.selected_category_id = null;
	this.selected_category_name = null;
	this.selected_month_id = null;
	this.monthList = [];
	this.categoryList = [];

	scope.changeCat = function() {
		scope.$broadcast("filterChanged");
	};

	scope.changeMonth = function() {
		scope.$broadcast("monthChanged");
		scope.$broadcast("filterChanged");
	};

	return this;
}


function ProductsCtrl($scope, $http, $window, $timeout, QAConfigs, ProductsAPI, QAUtils ) {

	$scope.filtermodel = FilterModel($scope);
	$scope.prodList;
	$scope.prodCount = 0;
	$scope.predicate = 'stores_count';
    $scope.reverse = true;
    $scope.top_products_count = 3;
    $scope.bottom_products_count = 3;

	$scope.$on("filterChanged", function () {		
		$scope.searchByFilter();
	});

	$scope.$on("monthChanged", function () {
		if (!$scope.filtermodel.selected_category_id) {
			$scope.prodList = null;
		}
		ProductsAPI.getAllProdsCountByMonthId($scope.filtermodel.selected_month_id).success(function(data, status, headers, config) {
				if(data) {
					$scope.prodCount = data;
				}
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::prods getAllProdsCountByMonthId");
	    	});
	});

	$scope.threeByStores = function(dir) {
		var m_id = $scope.filtermodel.selected_month_id;
		if (m_id) {
			QAUtils.block();
			var count = $scope.top_products_count;
			if (dir == 'bottom') {count = $scope.bottom_products_count;}
			ProductsAPI.showThreeProds(dir, m_id, count).success(function(data, status, headers, config) {
				if(data) {
					$scope.prodList = data;
					$scope.filtermodel.selected_category_id = null;
					$scope.filtermodel.selected_category_name = dir+" " + count + " products by stores count";
				} else {
					QAUtils.noty('error', 'No data found');
				}
		  		QAUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::prods searchByFilter");
	        	QAUtils.unblock();
	    	});
  		}
	};

	$scope.searchByFilter = function() {
		var obj = new Object();
		obj.category_id = $scope.filtermodel.selected_category_id;
		obj.month_id = $scope.filtermodel.selected_month_id;
		
		if (obj.category_id && obj.month_id) {
			QAUtils.block();
			ProductsAPI.showProdsByFilter(obj).success(function(data, status, headers, config) {
				if(data) {
					$scope.prodList = data;
				} else {
					QAUtils.noty('error', 'No data found');
				}
		  		QAUtils.unblock();
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::prods searchByFilter");
	        	QAUtils.unblock();
	    	});
  		}
	};

    $scope.showItem = function(id, name) {
    	$scope.filtermodel.selected_category_id = id;
    	$scope.filtermodel.selected_category_name = name;
    	$scope.$broadcast("filterChanged");
    };

    $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

	$scope.init = function() {
		angular.forEach($window.categoryData, function(item, ind){
			$scope.filtermodel.categoryList.push(item);
		});
		angular.forEach($window.monthData, function(item, ind){
			$scope.filtermodel.monthList.push(item);
		});
		$scope.filtermodel.selected_month_id = $window.lastMonthId;
		$scope.prodCount = $window.prodCount;
	};
}

