
//////////////////////// JS class for canvas ////////////////////////////////////

var qaApp = angular.module("qaApp", []);

qaApp.factory("QAConfigs", function($window){
  return {
            BASE_INDEX : $window.BASE_INDEX,
            BASE_URL   : $window.BASE_URL,
            MEDIA_URL  : $window.MEDIA_URL
        }; 
  });

  qaApp.factory("QAUtils" , function($window){
    var self =  {
        block : function(){
          //backgroundColor: '#f00', color: '#fff'
          jQuery.blockUI({
            showOverlay: false,  
            css: {
                  border: 'none', 
                  padding: '15px', 
                  backgroundColor: '#000', 
                  '-webkit-border-radius': '10px', 
                  '-moz-border-radius': '10px', 
                  opacity: .5, 
                  color: '#fff' 
               } 
          }); 
        },

        unblock : function(){
          jQuery.unblockUI({ fadeOut: 200 }); 
        },

        action: function(responseData){
            if(responseData && responseData["module"])
            {
              var moduleName = responseData["module"];
              switch(moduleName)
              {
                case "noty" :
                    self.noty(responseData.action, responseData.message);
                    break;
                case "redirect" :
                    self.redirect(responseData.action, responseData.message);
              }
            }

            return true;
        },

        redirect : function(action, path){
           $window.location.href = path;
        },

        noty : function(action,message){
          if(!action)
          {
              return;
          }

          var notyOb = null;
          switch(action)
          {
              case 'success':
                message = typeof message !== 'undefined' ? message : "Everything is OK";
                notyOb = $window.noty({text: message, layout: 'topRight', type: 'success', closeWith: ['click', 'hover'] });
                //notyOb = $window.noty({text: message, layout: 'topRight', type: 'success', timeout : true, closeWith: ['click', 'hover'] });
                break;
              case 'error':
                message = typeof message !== 'undefined' ? message : "Something wnet WRONG";
                //notyOb = $window.noty({text: message, layout: 'topRight', type: 'error', timeout : true, closeWith: ['click', 'hover']});
                notyOb = $window.noty({text: message, layout: 'topRight', type: 'error', closeWith: ['click', 'hover']});
                break;
          }
        }
    };

    return self;
  });

function MainCtrl($scope, $http, $window) 
{
	$scope.isAutorized;

	$scope.init = function() {
		$scope.isAutorized = $window.isAutorized;
	};
}

