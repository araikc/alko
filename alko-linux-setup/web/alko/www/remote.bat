python.exe C:\workspace\branch\4.0\GUI_Automation\bin\prepare_to_run.py -a DC -sd C:\workspace\branch\4.0\GUI_Automation -id C:\BUILDS > NUL 2>&1
python.exe C:\workspace\branch\4.0\GUI_Automation\bin\generate_tests.py -x C:\workspace\branch\4.0\GUI_Automation\iq_edge_tests.xls -vx C:\workspace\branch\4.0\GUI_Automation\variables.xls -o C:\workspace\branch\4.0\GUI_Automation\DesktopClient -s suite_Account_Balances -a DC  -f > NUL 2>&1
@del prepare
@type NUL > start
@set /p INST_DIR=<inst_dir
python.exe C:\workspace\branch\4.0\GUI_Automation\bin\run.py DC -ad "C:\BUILDS\QuestradeIQEdge\%INST_DIR%"  -sd  "C:\Squish\squish-5.0-20131129-1049-qt48x-win32-msvc11"  -s suite_Account_Balances -i "C:\workspace\branch\4.0\GUI_Automation\DesktopClient" -st "C:\workspace\branch\4.0\GUI_Automation\stub_pages\sq5vqlshr008" -o "C:\workspace\branch\4.0\GUI_Automation\reports"  -dl 1 -sto > NUL 2>&1
@FOR /f "tokens=*" %%a in ('dir C:\workspace\branch\4.0\GUI_Automation\reports\DC* /b /o') DO @set last=%%a
@xcopy C:\workspace\branch\4.0\GUI_Automation\reports\%last%  reports\%last% /E /I /Y
@del start