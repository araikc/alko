<?php

class ProductsModel extends CI_Model {

	public static $table = "products";

	function __construct()
	{
		parent::__construct();
		//$this->load->helper('array');
		$this->load->database();
	}
	
	public function getCount($m_id)
	{
		try
		{
			$data = null;
			$sql = "SELECT count(id) as count
					FROM ".self::$table." WHERE month_id=? LIMIT 1";

			$query = $this->db->query($sql, array($m_id));
				
			if($query->num_rows() > 0)
			{
				$data = $query->row_array();
			}
				
			$this->db->close();
			return intval($data['count']);
		}
		catch (Exception $e)
		{
		}
	}

	public function getProductsDataByFilter($category_id, $month_id, $pn, $pp)
	{
		try
		{
			$data = null;
			$sql_part = array();
			$q_arr = array();

			if ($category_id != '' && $category_id != 0) 
			{
				array_push($sql_part, " `category_id` = ?");
				array_push($q_arr, $category_id);
			}
			if ($month_id != '' && $month_id != 0)
			{
				array_push($sql_part, " `month_id` = ?");
				array_push($q_arr, $month_id);
			}
			
			$offset = $pp * ($pn-1);
			$limit  = $pp;
				
			$where = " WHERE ".implode(" AND ", $sql_part);

			$sql = "SELECT  id,
							name,
							category_id,
							country,
							price,
							stores_count,
							all_items_count,
							month_id

					FROM ".self::$table;  # " ID = ? AND application = ?";
				
			$sql = $sql.$where." ORDER BY stores_count DESC"." LIMIT ".$offset.", ".$limit;
			
			$query = $this->db->query($sql, $q_arr);

			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
			}
	
			$this->db->close();
			return $data;
		}
		catch (Exception $e)
		{
			log_message("error", "ProductsModel->getProductsDataById");
		}
	}
	
	public function three_by_stores($direction, $m_id, $count)
	{
		try
		{
			$dir = 'DESC';
			if ($direction == 'top') {
				$dir = 'DESC';
			} elseif ($direction == 'bottom') {
				$dir = 'ASC';
			}
			$data = array();
			$sql = "SELECT  id,
							name,
							category_id,
							country,
							price,
							stores_count,
							all_items_count,
							month_id

					FROM ".self::$table." WHERE month_id = ? ORDER BY stores_count ".$dir." LIMIT ".$count;
			$query = $this->db->query($sql, array($m_id));
				
			if($query->num_rows() > 0)
			{
				foreach ($query->result_array() as $row)
				{
					$data[] = $row;
				}
			}

			$this->db->close();
			return $data;
		}
		catch (Exception $e)
		{
			log_message("error", "ProductsModel->get_prods_by_dir");
		}
	}

	public function getAllProducts()
	{
		try
		{
			$data = array();
			$sql = "SELECT  id,
							name,
							category_id,
							country,
							price,
							stores_count,
							all_items_count,
							month_id

					FROM ".self::$table." ORDER BY stores_count DESC";
			$query = $this->db->query($sql);
				
			if($query->num_rows() > 0)
			{
				foreach ($query->result_array() as $row)
				{
					$data[] = $row;
				}
			}

			$this->db->close();
			return $data;
		}
		catch (Exception $e)
		{
			log_message("error", "ProductsModel->getAllProducts");
		}
	}
}
