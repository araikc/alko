<?php

class CommonModel extends CI_Model {

	public static $cattable   = "categories";
	public static $montable   = "months";
	public static $prodtable   = "products";

	function __construct()
	{
		parent::__construct();
		//$this->load->helper('array');
		$this->load->database();
	}

	public function getAllCategories()
	{
		try 
		{
			$data = null;
			$sql = "SELECT * FROM ".self::$cattable;
			
			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
			}
			
			$this->db->close();
			return $data;
		}
		catch(Exception $e)
		{
			log_message('error', "QAModel->getAllCategories");
		}
	}

	public function getAllMonths()
	{
		try 
		{
			$data = null;
			$sql = "SELECT * FROM ".self::$montable;
			
			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
			}
			
			$this->db->close();
			return $data;
		}
		catch(Exception $e)
		{
			log_message('error', "QAModel->getAllMonths");
		}
	}

	public function getLastMonthId()
	{
		try 
		{
			$data = null;
			$sql = "SELECT `id` FROM ".self::$montable . " order by `id` DESC LIMIT 1";
			
			$query = $this->db->query($sql);
			
			if($query->num_rows() > 0)
			{
				$data = $query->row_array();
			}
			
			$this->db->close();
			return intval($data['id']);
		}
		catch(Exception $e)
		{
			log_message('error', "QAModel->getAllMonths");
		}
	}

	public function getCatProdCountByMonths($m_id) 
	{
		try 
		{
			$data = null;
			$sql = "SELECT C.id, C.name, count(P.id) as qty
					FROM ".self::$cattable." as C left join 
					".self::$prodtable . " as P 
					on P.category_id = C.id and P.month_id = ?
					group by C.id
					order by C.id ASC";
			
			$query = $this->db->query($sql, array($m_id));
			
			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
			}
			
			$this->db->close();
			return $data;
		}
		catch(Exception $e)
		{
			log_message('error', "QAModel->getAllMonths");
		}
	}

	public function getProdStoresQtyByMonths($cat_id, $m_id) 
	{
		try 
		{
			$data = null;
			$sql = "SELECT P.id, P.name, P.stores_count as qty
					FROM ".self::$prodtable." as P 
					WHERE P.category_id = ? and P.month_id = ?
					ORDER BY P.id ASC";
			
			$query = $this->db->query($sql, array($cat_id, $m_id));
			
			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
			}
			
			$this->db->close();
			return $data;
		}
		catch(Exception $e)
		{
			log_message('error', "QAModel->getAllMonths");
		}
	}
	
}
