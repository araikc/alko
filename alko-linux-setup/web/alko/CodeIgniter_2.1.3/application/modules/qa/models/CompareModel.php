<?php

class CompareModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
		//$this->load->helper('array');
		$this->load->database();
	}

	public function getComparisonData($rid1, $rid2)
	{
		$data1 = $this->getRunData((int)$rid1);
		$data2 = $this->getRunData((int)$rid2);
		$cmp_data = $this->compareData($data1, $data2);
		return $cmp_data;
	}

	public function getRunData($rid)
	{
		$retdata = array();
		$retdata['rdata'] = array();
		$retdata['sdata'] = array();
		$retdata['tdata'] = array();
		$rdata = $this->RegressionsModel->getRegressionDataById($rid);
		$sdata = $this->SuitesModel->getSuitesByRunId($rid);
		
		$sData = array();
		foreach($sdata as $s) {
			$sData[] = $s['S_ID'];
		}
		$sdata = array_combine($sData, $sdata);
		$tdata = array();
		foreach($sdata as $sk => $sv) {
			$ctdata = $this->TestsModel->getTestsBySuiteId($sk, $rid);
			$tdata = array_merge($tdata, $ctdata);
		}
		$tData = array();
		foreach($tdata as $t) {	$tData[] = $t['T_ID'];	}
		$tdata = array_combine($tData, $tdata);
		$retdata['rdata'] = $rdata;
		$retdata['sdata'] = $sdata;
		$retdata['tdata'] = $tdata;
		
		return $retdata;
	}


	/*
		Usable for suites and tests
	 */
	public function compareData($data1, $data2)
	{
		
		$ret_data = array(

			'DELETED' => array(),

			'ADDED' => array(
				'PASSED' => array(),
				'FAILED' => array()
				),

			'BOTH_PASSED' => array(),

			'BOTH_FAILED' => array(
				'SAME' => array(),
				'DIFF' => array()
				),
			
			'PASSED_TO_FAILED' => array(),
			'FAILED_TO_PASSED' => array()
			
		);

		if(isset($data1)) {
			foreach ($data1 as $t => $d1)
			{
				if (array_key_exists($t, $data2) == false)
				{
					$ret_data['DELETED'][$t] = $d1;
				}
				else
				{
					$status1 = $d1['status'];
					$status2 = $data2[$t]['status'];
					
					if ($status1 == 2 && $status2 == 2) // Both passed DB-um pass = 2
					{
						$ret_data['BOTH_PASSED'][$t] = array();
						$ret_data['BOTH_PASSED'][$t]['DATA1'] = $d1;
						$ret_data['BOTH_PASSED'][$t]['DATA2'] = $data2[$t];
					}
					elseif ($status1 == 2 && $status2 != 2) // Passed to failed
					{
						$ret_data['PASSED_TO_FAILED'][$t] = array();
						$ret_data['PASSED_TO_FAILED'][$t]['DATA1'] = $d1;
						$ret_data['PASSED_TO_FAILED'][$t]['DATA2'] = $data2[$t];
					}
					elseif ($status1 != 2 && $status2 == 2) // Failed to passed
					{
						$ret_data['FAILED_TO_PASSED'][$t] = array();
						$ret_data['FAILED_TO_PASSED'][$t]['DATA1'] = $d1;
						$ret_data['FAILED_TO_PASSED'][$t]['DATA2'] = $data2[$t];
					}
					else // Both failed
					{
						//$reason1 = $d1[1];
						//$reason2 = $data2[$t][1];

						$ret_data['BOTH_FAILED']['DIFF'][$t] = array();
						$ret_data['BOTH_FAILED']['DIFF'][$t]['DATA1'] = $d1;
						$ret_data['BOTH_FAILED']['DIFF'][$t]['DATA2'] = $data2[$t];
					}	
					unset($data2[$t]);
				}
				unset($data1[$t]);
			}
	    } // if(isset($data1))

	    if(isset($data2)) {
			foreach ($data2 as $t => $d2)
			{
				if ($d2[0] == 'PASSED')
				{
					$ret_data['ADDED']['PASSED'][$t] = $d2;
				}
				else
				{
					$ret_data['ADDED']['FAILED'][$t] = $d2;			
				}
				unset($data2[$t]);
			}
	    }
		return $ret_data;
	}

}
