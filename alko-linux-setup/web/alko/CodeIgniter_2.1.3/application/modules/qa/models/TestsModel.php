<?php

class TestsModel extends CI_Model {

	public static $table = "tests";

	function __construct()
	{
		parent::__construct();
		//$this->load->helper('array');
		$this->load->database();
	}

	public function getTestsBySuiteId($sid, $rid)
	{
		try 
		{
			$data = null;
			$sql = "SELECT * FROM ".self::$table." as T INNER JOIN testdata as TD WHERE T.T_ID = TD.ID AND T.S_ID = ? AND T.R_ID = ?";
			$query = $this->db->query($sql, array($sid, $rid));
			
			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
			}
			
			$this->db->close();
			return $data;
		}
		catch(Exception $e)
		{
			log_message('error', "SuitesModel->getTestsBySuiteId");
		}
	}
	
}
