<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oauth extends MX_Controller {
	
	//log_message('error', 'message test');
	
	private $isAuthorized;
	private $data;
	
	public function __construct()
	{
		parent::__construct();
		
		qa_session::init();
		
		$this->data = array();
		$this->data[PRODUCTS]		= PRODUCTS;
		
		$this->isAuthorized = false;
	}
	
	public function islogged()
	{
		$this->isAuthorized = qa_session::get('logged');
		if(!$this->isAuthorized)
		{
			qa_utils::locationRedirect(BASE_INDEX . 'qa/main/signin');
			exit();
		}
		else 
		{
			return 1; // authorized
		}
	}
	
	public function login()
	{
		if($this->input->post('user') == "user" && $this->input->post('pass') == "pass")
		{
			qa_session::set('logged', 1);
			qa_utils::evalRedirect(BASE_INDEX .'qa/main/index');
		}
		else
		{
			qa_utils::evalRedirect(BASE_INDEX . 'qa/main/signin');
			exit();
		}
	}

	public function logout()
	{
		
		unset($_SESSION['logged']);
		$this->islogged();
	}	
	
}
