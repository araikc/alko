<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tests extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!session_id())
		{
			session_name(SESSION_NAME);
			session_start();
		}

		Modules::run('qa/oauth/islogged');

		$this->data = array();
		
		$this->load->model("RegressionsModel");
		$this->load->model("SuitesModel");
		$this->load->model("TestsModel");
		//$this->load->model("TestPointsModel");
	}
	
}

