<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MX_Controller {
	
	private $data;
	
	public function __construct()
	{
		parent::__construct();
		
		if(!session_id())
		{
			session_name(SESSION_NAME);
			session_start();
		}
		
		$_SESSION['pp'] = PP;
		
		$this->data = array();
		$this->data[PRODUCTS]		    = PRODUCTS;
		$this->data[MONTHS]				= MONTHS;
		
		#Modules::run('qa/oauth/islogged');
		
	}
	
	public function index()
	{
		$this->data['CONTENT'] = $this->load->view("index.php", '', true);
		$this->load->view("qa/main_view.php", $this->data);
	}
	
	
	public function show($page = null, $param = null)
	{
		$pageData = array();
		$this->data['isAutorized'] = Modules::run('qa/oauth/islogged');
		
		switch($page)
		{
			case PRODUCTS:
				$this->load->model("ProductsModel");
				$this->load->model("CommonModel");

				$pageData['lastMonthId'] 	 = json_encode($this->CommonModel->getLastMonthId());
				$m_id = $pageData['lastMonthId'];
				$pageData['prodCount'] 		 = json_encode($this->ProductsModel->getCount($m_id));
				$pageData['categoryData'] 	 = json_encode($this->CommonModel->getAllCategories());
				$pageData['monthData'] 		 = json_encode($this->CommonModel->getAllMonths());
				break;

			case MONTHS:
				$this->load->model("ProductsModel");
				$this->load->model("CommonModel");
				
				$pageData['categoryData'] 	 = json_encode($this->CommonModel->getAllCategories());
				$pageData['monthData'] 		 = json_encode($this->CommonModel->getAllMonths());
				$pageData['lastMonthId'] 	 = json_encode($this->CommonModel->getLastMonthId());
				break;
			
			default:{ $this->index(); return;}
		}

		
		$this->data['CONTENT'] = $this->load->view("qa/" . $page . ".php", $pageData , true);
		$this->load->view("qa/main_view.php", $this->data);
	}


	public function signin()
	{
		$this->data['CONTENT'] = $this->load->view("qa/login_form.php", "" , true);
		$this->load->view("qa/main_view.php" ,$this->data);
	}
	
}
