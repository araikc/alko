<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MX_Controller {

	public function __construct()
	{
		qa_session::init();
		parent::__construct();

		Modules::run('qa/oauth/islogged');
		$this->load->model("ProductsModel");
	}

	public function showlist()
	{
        echo "Show products list";
	}

	public function get_prods_by_dir()
	{
		$post = $this->input->post();

		$dir = $post['dir'];
		$m_id = $post['month_id'];
		$count = $post['count'];

		$db = $this->ProductsModel->three_by_stores($dir, $m_id, $count);

		$data =array();
		if ($db) {
			$data = $db;
		}

		echo json_encode($data, JSON_NUMERIC_CHECK);
	}

	public function get_products_by_filter()
	{
		$filter = $this->input->post('filter');

		$data = array();
		$category_id = $filter['category_id'];
		$month_id = $filter['month_id'];
		$pp = 500;
		$pn = 1;

		$db = $this->ProductsModel->getProductsDataByFilter($category_id, $month_id, $pn, $pp);
		$data =array();
		if ($db) {
			$data = $db;
		}
		echo json_encode($data, JSON_NUMERIC_CHECK);
	}

	public function get_last_month_id() {
		$db = $this->ProductsModel->getLastMonthId();
		$data =array();
		if ($db) {
			$data = $db;
		}

		echo json_encode($data, JSON_NUMERIC_CHECK);
	}

	public function get_prods_count_by_month_id() {
		$m_id = $this->input->post('m_id');

		$data = $this->ProductsModel->getCount(intval($m_id));

		echo json_encode($data, JSON_NUMERIC_CHECK);
	}

}

