<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Months extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!session_id())
		{
			session_name(SESSION_NAME);
			session_start();
		}

		Modules::run('qa/oauth/islogged');

		$this->data = array();
		
		//$this->load->model("MonthsModel");
		$this->load->model("CommonModel");
	}
	
	public function get_prod_stores_count_by_month()
	{
		$postData = $this->input->post();
		$cat = $postData['cat'];
		$m1 = $postData['m1'];
		$m2 = $postData['m2'];

		$m1data = $this->CommonModel->getProdStoresQtyByMonths(intval($cat), intval($m1));
		$m2data = $this->CommonModel->getProdStoresQtyByMonths(intval($cat), intval($m2));
		
		$data = array();

		if ($m1data && $m2data) 
		{

			$m1data = $this->transform($m1data);
			$m2data = $this->transform($m2data);
			foreach ($m2data as $key => $val) {
				if (array_key_exists($key, $m1data)) {
					$m1data[$key]['qty'] = intval($m1data[$key]['qty']);
					$m1data[$key]['qty2'] = intval($val['qty']);
				} else {
					$m1data[$key] = $m2data[$key];
					$m1data[$key]['qty2'] = $m2data[$key]['qty'];
					unset($m1data[$key]['qty']);
				}
			}
		}

		echo json_encode($m1data, JSON_NUMERIC_CHECK);
	}	

	public function get_cat_prod_count_by_month()
	{
		$postData = $this->input->post();
		$m1 = $postData['m1'];
		$m2 = $postData['m2'];

		$m1data = $this->CommonModel->getCatProdCountByMonths(intval($m1));
		$m2data = $this->CommonModel->getCatProdCountByMonths(intval($m2));

		foreach ($m2data as $key => $val) {
			$m1data[$key]['qty'] = intval($m1data[$key]['qty']);
			$m1data[$key]['qty2'] = intval($val['qty']);
		}
		echo json_encode($m1data, JSON_NUMERIC_CHECK);
	}

	public function transform($data) {
		$ret = array();
		foreach($data as $ind => $row) {
			$ret[$row['name']] = $row;
		}
		return $ret;
	}
}

