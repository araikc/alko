
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php 
  	qa_asset_loader::jquery(true, true);
  	qa_asset_loader::angularjsVersion(true, "1.2.15");
  	qa_asset_loader::angularstrap();
  	qa_asset_loader::bootstrap();
  	qa_asset_loader::chosen();
  	qa_asset_loader::boostrapDateTimePicker();
  	qa_asset_loader::noty();

    script("vendorlib/blockUI/jqury.blockUI.js");
?>

<script>
    window.BASE_URL     = <?php echo '"' . BASE_URL  . '"'; ?>;
    window.INDEX_PHP    = <?php echo '"' . INDEX_PHP . '"'; ?>;
    window.BASE_INDEX   = <?php echo '"' . BASE_INDEX  . '"'; ?>;
    window.MEDIA_URL    = <?php echo '"' . MEDIA_URL . '"'; ?>;
</script>

<?php
  	## CSS ##
	css("css/qa.css");
	## JS ##
	script("js/ng_qamonitor.js");
	script("js/ng_directives.js");
?>

</head>

<body ng-app="qaApp" id="ng-app" >

<div class="navbar navbar-inverse" style="position: static;" ng-controller="MainCtrl" ng-init="init()">
              <div class="navbar-inner">
                <div class="container">
                  <a class="btn btn-navbar" data-toggle="collapse" data-target=".subnav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </a>
                  <a class="brand" href="/index.php/qa/main/index">Logo</a>
                  <div class="nav-collapse subnav-collapse">
                  
                    <ul class="nav">
                      <li><a href= "<?php echo '/index.php/qa/main/show/' .  PRODUCTS;    ?>" >Main</a></li>
                      <li><a href= "<?php echo '/index.php/qa/main/show/' .  MONTHS;    	   ?>" >Months</a></li>
                    </ul>
                   
                    <ul class="nav pull-right">
                                        
                      <li><a href="<?php echo '/index.php/qa/oauth/logout' ?>"> <i class="icon-off"></i> Log out</a></li>
                    </ul>
                  </div><!-- /.nav-collapse -->
                </div>
              </div><!-- /navbar-inner -->
</div>

<?php echo $CONTENT;?>

</body>
</html>
