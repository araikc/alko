<script>
	function SigninCntrl($scope, $http, QAConfigs, QAUtils)
	{
		$scope.signinModel = { "user" : "", "pass" : ""};

		$scope.signin = function(){
			$http({	method: "POST",
					url: QAConfigs.BASE_INDEX + "qa/oauth/login",
					data :  $.param($scope.signinModel),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	    	}).success(function(data, status, headers, config){
	  			QAUtils.action(data);
	  		}).error(function(data, status, headers, config) {
	        	console.log("js::SigninCntrl signin");
	    	});
		};
	}
</script>

<div class="container" style="margin-top:150px;"  ng-controller="SigninCntrl">
<div class="row">

<div class="span6 offset3">

<form class="well form-inline" method="POST">

  <input name = "user" type="text" class="input-small" placeholder="Username" style="height:25px;width:150px;" ng-model="signinModel.user">
  <input name = "pass" type="password" class="input-small" placeholder="Password" style="height:25px;width:150px;" ng-model="signinModel.pass">
  
  <button name="signin"   ng-click="signin()" data-loading-text="Signing..." class="btn btn-primary" type="submit" style="float:right;"> Sign In </button>
  
</form>

</div>

</div>
</div>


