
<?php script("js/ng_months.js"); ?>

<script>
	var categoryData   = <?php echo $categoryData; ?>;
	var monthData   = <?php echo $monthData; ?>;
	var lastMonthId = <?php echo $lastMonthId; ?>;
</script>

<style>
.nut-sidenav {
  padding: 0;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  -webkit-box-shadow: 0 1px 4px rgba(0,0,0,.065);
     -moz-box-shadow: 0 1px 4px rgba(0,0,0,.065);
          box-shadow: 0 1px 4px rgba(0,0,0,.065);
}

.nut-sidenav > li > a {
  background-color: #fff;
  display: block;
  width: 190px \9;
  margin: 0 0 -1px;
  padding: 8px 14px;
  border: 1px solid #e5e5e5;
}

.nut-sidenav > li:first-child > a {
  -webkit-border-radius: 6px 6px 0 0;
     -moz-border-radius: 6px 6px 0 0;
          border-radius: 6px 6px 0 0;
}
.nut-sidenav > li:last-child > a {
  -webkit-border-radius: 0 0 6px 6px;
     -moz-border-radius: 0 0 6px 6px;
          border-radius: 0 0 6px 6px;
}

.nut-sidenav > li > a:hover {
  background-color: #f5f5f5;

}

.nut-sidenav  .check {
  float: right;
  margin-top: 2px;
  margin-right: -6px;
  opacity: 1;
}

.nut-sidenav a:hover .check {
  opacity: 1;
}

.nut-sidenav > .active > a {
  position: relative;
  z-index: 2;
  padding: 9px 15px;
  border: 0;
  text-shadow: 0 1px 0 rgba(0,0,0,.15);
  -webkit-box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
     -moz-box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
          box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
}

.nut-sidenav .active .check,
.nut-sidenav .active a:hover .check {
  background-image: url(../img/glyphicons-halflings-white.png);
  opacity: 1;
}

.selMonth {
	width: 90px;
	height: 30px;
	border: dashed 2px gray;
	padding: 5px;
	border-radius: 16px 16px 16px 16px;
	-moz-border-radius: 16px 16px 16px 16px;
	-webkit-border-radius: 16px 16px 16px 16px;
	border: 8px outset #a3a0a3;
	text-align: center;
	vertical-align: center;
	font-size: 18px;
	font-weight: bold;
}


.vs {
	float:left; margin-left:20px; margin-top:20px;
  color: #000000;
  font-family: helvetica;
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  font-variant: normal;
}
</style>

<div ng-controller= "MonthsCtrl">
	<div ng-init="init()"></div>
	
	<div class="row-fluid">

		<div class="span3">
			<div class="row-fluid" style="height: 500px; overflow: auto;">
				<div class="span12 well">
					<div style="position:relative;">
						<span class="label label-warning" style="float:left; font-size: 20px; padding:8px;">Select Months</span>
						<button style="float:left; margin-left:25px" class="btn btn-success" ng-click="showDiff()">Compare</button>
					</div>
					<div style="clear:both; margin-top:40px">
						<h3 ng-show="monthList.length == 0" class="ng-hide">No months</h3>
						<ul class="nav nav-pills nav-stacked nut-sidenav">
							<li ng-repeat="item in monthList">
								<!-- <a href="#" ng-click="selectItem(item.id, $event)"><input class="check" disabled type="checkbox"></input>{{item.name}}</a> -->
								<a href="#" ng-click="selectItem(item.id, item.name, $event);"><span class="check label label-info"></span>{{item.name}}</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="span9">
			<div class="span8 well" >

				<div style="width:360px; margin:0 auto">
					<div>
						<div class="selMonth" style="float:left;">{{compModel.firstName}}</div> 
						<div class="vs">VS</div> 
						<div class="selMonth" style="float:left; margin-left:20px">{{compModel.secondName}}</div>
					</div>
				</div>
				<div class="row-fluid"></div>
				<hr style="clear:both;height:2px; background-color:#ffffff" >
				<div ng-show="compModel.firstName && compModel.secondName && compModel.ready">
				<div class="categoryInfo">
					<div class="row-fluid">
						<select style="width:120px; float:left" ngwidth="100%" ng-model="compModel.selCondition"
								ng-options="m.id as m.name for m in compModel.prodCount"
								ng-change="changeProdCount()">
					    </select>
						<div style="float:left; margin-left: 30px">
					    	<span class="label label-warning" style="float:left; font-size: 16px; padding:8px;margin-right:4px">Sort by diff</span>
						    <select style="width:132px" ngwidth="100%" ng-model="diffType1"
									ng-options="m.type as m.value for m in diffTypes1"
									ng-change="sort1()">
						    </select>
						</div>					    
					</div>
					<div class="row-fluid">
						<div class="span3">
							<div class="input-append span12">
								<input type="text" class="ng-pristine ng-valid" ng-model="searchtxt" placeholder="Type category name">
								<span class="add-on"><i class="icon-search"></i></span>
							</div>
						</div>
					</div>
					<div class="row-fluid" style="height: 300px; overflow: auto;">
						<table class="table table-hover table-striped table-bordered table-condensed" ng-hide="prodsByCat.length == 0" >
								<thead>
									<th style="width:100px">Category</th>
									<th style="width:50px">Products Qty M1</th>
									<th style="width:50px">Products Qty M2</th>
								</thead>
								<tbody >
									<tr ng-show="isShow(item.data)" ng-repeat="item in prodsByCatSort| filter:searchtxt">
										<td style="width:50px">{{item.data.name}}</td>
										<td style="width:50px">{{item.data.qty}}</td>
										<td style="width:50px">{{item.data.qty2}}</td>
									</tr>
								</tbody>
							</table>
					</div>
				</div>
				<hr style="clear:both;height:2px; background-color:#ffffff" >
				<div class="productInfo">
					<div class="row-fluid">
						<select placeholder="Select category" style="width:132px; float:left" ngwidth="100%" ng-model="compModel.selCatId"
								ng-options="m.id as m.name for m in compModel.categoryList"
								ng-change="selectCategory()">
					    </select>
					    <select style="width:132px; float:left" ngwidth="100%" ng-model="compModel.selInfo"
								ng-options="m.id as m.name for m in compModel.storeCount">
					    </select>
					    <div style="float:left; margin-left: 30px">
					    	<span class="label label-warning" style="float:left; font-size: 16px; padding:8px;margin-right:4px">Sort by diff</span>
						    <select style="width:132px" ngwidth="100%" ng-model="diffType2"
									ng-options="m.type as m.value for m in diffTypes2"
									ng-change="sort2()">
						    </select>
						</div>
					</div>
<!-- 					<div class="row-fluid">
						<div class="span3">
							<div class="input-append span12">
								<input type="text" class="span10 ng-pristine ng-valid" ng-model="searchtxt" placeholder="Type category name">
								<span class="add-on"><i class="icon-search"></i></span>
							</div>
						</div>
					</div> -->
					<div class="row-fluid" style="height: 300px; overflow: auto;">
						<table class="table table-hover table-striped table-bordered table-condensed">
								<thead>
									<th style="width:100px">Name</th>
									<th style="width:50px">Stores Qty M1</th>
									<th style="width:50px">Stores Qty M2</th>
								</thead>
								<tbody >
									<tr ng-show="isGainLose(item.data)" ng-repeat="item in storesByMonthSort">
										<td style="width:50px">{{item.data.name}}</td>
										<td style="width:50px">{{item.data.qty}}</td>
										<td style="width:50px">{{item.data.qty2}}</td>
									</tr>
								</tbody>
							</table>
					</div>
				</div>
				</div>

			</div>
			
		</div>
	</div>
</div>

