
<?php script("js/ng_products.js"); ?>

<script>
	var categoryData   = <?php echo $categoryData; ?>;
	var monthData   = <?php echo $monthData; ?>;
	var prodCount = <?php echo $prodCount; ?>;
	var lastMonthId = '<?php echo $lastMonthId; ?>';

</script>

<style>
.nut-sidenav {
  padding: 0;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  -webkit-box-shadow: 0 1px 4px rgba(0,0,0,.065);
     -moz-box-shadow: 0 1px 4px rgba(0,0,0,.065);
          box-shadow: 0 1px 4px rgba(0,0,0,.065);
}

.nut-sidenav > li > a {
  background-color: #fff;
  display: block;
  width: 190px \9;
  margin: 0 0 -1px;
  padding: 8px 14px;
  border: 1px solid #e5e5e5;
}

.nut-sidenav > li:first-child > a {
  -webkit-border-radius: 6px 6px 0 0;
     -moz-border-radius: 6px 6px 0 0;
          border-radius: 6px 6px 0 0;
}
.nut-sidenav > li:last-child > a {
  -webkit-border-radius: 0 0 6px 6px;
     -moz-border-radius: 0 0 6px 6px;
          border-radius: 0 0 6px 6px;
}

.nut-sidenav > li > a:hover {
  background-color: #f5f5f5;

}

.nut-sidenav  .icon-chevron-right {
  float: right;
  margin-top: 2px;
  margin-right: -6px;
  opacity: .25;
}

.nut-sidenav a:hover .icon-chevron-right {
  opacity: 0.5;
}

.nut-sidenav > .active > a {
  position: relative;
  z-index: 2;
  padding: 9px 15px;
  border: 0;
  text-shadow: 0 1px 0 rgba(0,0,0,.15);
  -webkit-box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
     -moz-box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
          box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
}

.nut-sidenav .active .icon-chevron-right,
.nut-sidenav .active a:hover .icon-chevron-right {
  background-image: url(../img/glyphicons-halflings-white.png);
  opacity: 1;
}

.prod_table th {
	cursor: pointer; cursor: hand;
}

.sortorder:after {
    content: '\25b2';
}

.sortorder.reverse:after {
    content: '\25bc';
}

</style>

<div ng-controller= "ProductsCtrl" id="prodView">
	<div ng-init="init()"></div>
	
	<div class="row-fluid">

		<div class="span3">
			<div class="row-fluid">
				<div class="span12 well">
					<div class="input-append span12">
						<input type="text" class="span10 ng-pristine ng-valid" ng-model="searchtxt" placeholder="Type category name">
						<span class="add-on"><i class="icon-search"></i></span>
					</div>
				</div>
			</div>
			<div class="row-fluid" style="height: 700px; overflow: auto;">
				<div class="span12 well">
					<h3 ng-show="filtermodel.categoryList.length == 0" class="ng-hide">No Categories</h3>
					<ul id="" class="nav nav-pills nav-stacked nut-sidenav">
						<li ng-repeat="item in filtermodel.categoryList | filter:searchtxt" ng-class="{active: item.id==filtermodel.selected_category_id}">
							<a href="#" ng-click="showItem(item.id, item.name)"><i class="icon-chevron-right"></i>{{item.name}}</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="span9">

			<div class="row-fluid">
				<div class="span12 well" style="height:80px">

					<div style="float:left; position:relative; padding-left:20px;">
							<h5 style="float:left;">All products :
							<span class="badge badge-important">{{prodCount}}</span>
							</h5>
					</div>

					<div style="float:left; position:relative; padding-left:50px; padding-top:10px">
							<a class="btn btn-success" href="#" style="margin-right:10px; float:left" ng-click="threeByStores('top')">Top</a>
							<input type="text" style="width:25px; float:left" ng-model="top_products_count">
							<span style="float:left; margin-top:5px; margin-left:5px">products by Stores QTY</span>
					</div>
					<div style="float:left; position:relative; padding-left:30px; padding-top:10px">
							<a class="btn btn-danger" href="#" style="margin-right:10px; float:left" ng-click="threeByStores('bottom')">Bottom</a>
							<input type="text" style="width:25px; float:left" ng-model="bottom_products_count">
							<span style="float:left; margin-top:5px; margin-left:5px">products by Stores QTY</span>
					</div>

					<div class="pull-right">
							<h5 style="float:left; padding-right:20px">Month</h5>
							<select style="width:132px" ngwidth="100%" data-placeholder="Choose Month" ng-model="filtermodel.selected_month_id"
									ng-options="m.id as m.name for m in filtermodel.monthList"
									ng-change="changeMonth()">
						    </select>
					</div>
				</div>
			</div>
			<div class="row-fluid" ng-show="prodList">
				<div class="span12 well">
					<div><span class="label label-info" style="font-size: 20px; padding:8px; margin-bottom:5px; margin-top:-10px">{{filtermodel.selected_category_name}}</span></div>
					<table class="table table-hover table-striped table-bordered table-condensed prod_table" ng-hide="prodList.length == 0" >
						<thead>
							<th style="width:100px" ng-click="order('name')">Name <span class="sortorder" ng-show="predicate === 'name'" ng-class="{reverse:reverse}"></span></th>
							<th style="width:50px" ng-click="order('country')">Country <span class="sortorder" ng-show="predicate === 'country'" ng-class="{reverse:reverse}"></span></th>
							<th style="width:50px" ng-click="order('price')">Price (&#8364;) <span class="sortorder" ng-show="predicate === 'price'" ng-class="{reverse:reverse}"></span></th>
							<th style="width:50px" ng-click="order('stores_count')">Stores QTY <span class="sortorder" ng-show="predicate === 'stores_count'" ng-class="{reverse:reverse}"></span></th>
							<th style="width:20px" ng-click="order('all_items_count')">All bottles QTY <span class="sortorder" ng-show="predicate === 'all_items_count'" ng-class="{reverse:reverse}"></span></th>
						</thead>
						<tbody >
							<tr ng-repeat="item in prodList | orderBy : predicate : reverse">
								<td style="width:50px">{{item.name}}</td>
								<td style="width:50px">{{item.country}}</td>
								<td style="width:50px">{{item.price}}</td>
								<td style="width:50px">{{item.stores_count}}</td>
								<td style="width:50px">{{item.all_items_count}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

<!-- 	<div class="row-fluid">

		<div ng-show="!regList">There are no any regression result !</div>

		<div class="pagination pull-right">
	        <ul>
	            <li ng-class="{disabled: currentPage == 1}">
	                <a href ng-click="prevPage()">« Prev</a>
	            </li>
	            <li ng-repeat="n in pages"
	                ng-class="{active: n == currentPage}"
	            	ng-click="setPage(n)">
	                <a href >{{n}}</a>
	            </li>
	            <li ng-class="{disabled: currentPage == numPages}">
	                <a href ng-click="nextPage()">Next »</a>
	            </li>
	        </ul>
	    </div>
	</div> -->


</div>

