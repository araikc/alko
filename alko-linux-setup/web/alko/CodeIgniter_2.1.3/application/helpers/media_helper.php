<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//$ci =& get_instance();

if ( ! function_exists('getPratacol'))
{
	function getPratacol()
	{
		return PRATACOL;
	}
}


if ( ! function_exists('css'))
{
	 function css($name)
	{
		echo '<link rel="stylesheet" type="text/css"  href="' . MEDIA_URL . $name .  '">';
	}
}

if ( ! function_exists('script'))
{
	function script($name)
	{
		echo '<script type= "text/javascript" src="' . MEDIA_URL . $name .  '"></script>';
	}
}

if ( ! function_exists('media_path'))
{
	function media_path($name)
	{
		return MEDIA_URL . $name;
	}
}