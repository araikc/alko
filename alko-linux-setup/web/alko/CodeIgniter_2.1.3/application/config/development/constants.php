<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */

define("SESSION_NAME", "PHPSESSID");

define("LIB_QA", "qa/core/");
define("VENDORLIB",      "vendorlib");

// pages
define("PRODUCTS", "products");
define("MONTHS", "months");
define("PP", "13pp");
define("PAGES_COUNT", 3);

// Runner global variables
define("QA_ENV_DIR", "C:\\workspace\\branch\\4.0\\GUI_Automation");
#define("AUT_DIR", "C:\\BUILDS\\DesktopClient\\Questrade IQ Edge QA-4.0.18");
define("SQUISH_DIR", "C:\\Squish\\squish-5.0-20131129-1049-qt48x-win32-msvc11");
define("INPUT_DIR", QA_ENV_DIR."\\DesktopClient");
define("STUB_DIR", QA_ENV_DIR."\\stub_pages");
define("OUT_DIR", QA_ENV_DIR."\\reports");
define("INSTALL_DIR", "C:\\BUILDS");
define("SVN_DIR", "C:\\workspace\\branch\\4.0\\GUI_Automation");

