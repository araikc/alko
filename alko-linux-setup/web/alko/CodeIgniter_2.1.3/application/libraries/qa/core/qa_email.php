<?php
//namespace nut\core;

	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class qa_email{
	//The following is a list of all the preferences that can be set when sending email.
	// have a look http://ellislab.com/codeigniter/user-guide/libraries/email.html  for more details
		
		private static $default_config = array(
			"useragent" =>	"CodeIgniter",
			"protocol"  =>	"mail",  					// 	mail, sendmail, or smtp	The mail sending protocol.
			"mailpath"  =>	"/usr/sbin/sendmail",   	//  None	The server path to Sendmail.
			"smtp_host"	=>	"",							//  No Default	None	SMTP Server Address.
			"smtp_user"	=>	"",							//  No Default	None	SMTP Username.
			"smtp_pass" =>  "",	 						//  No Default	None	SMTP Password.
			"smtp_port"	=>  25,  						//  25	None	SMTP Port.
			"smtp_timeout" => 5,						//  None	SMTP Timeout (in seconds).
			"wordwrap" =>	TRUE,						//  TRUE or FALSE (boolean)	Enable word-wrap.
			"wrapchars" =>	76,							//  Character count to wrap at.
			"mailtype" =>	"text",						//  text or html	Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
			"charset"  =>  "utf-8",						//  Character set (utf-8, iso-8859-1, etc.).
			"validate" =>	FALSE,						//  TRUE or FALSE (boolean)	Whether to validate the email address.
			"priority" => 3,							//  1, 2, 3, 4, 5	Email Priority. 1 = highest. 5 = lowest. 3 = normal.
			"crlf" => "\r\n",							//  \n	"\r\n" or "\n" or "\r"	Newline character. (Use "\r\n" to comply with RFC 822).
			"newline" => "\r\n",						//  \n	"\r\n" or "\n" or "\r"	Newline character. (Use "\r\n" to comply with RFC 822).
			"bcc_batch_mode" =>	FALSE,					//  TRUE or FALSE (boolean)	Enable BCC Batch Mode.
			"bcc_batch_size" =>	200						//  None	Number of emails in each BCC batch.
		);
		
		//codeigniter instance
		public $ci;
		private $m_data;
		
		// sent email data
		public $from_email="noreply@betchili.com";
		public $from_name = '';
		public $to_email = '';
		public $cc_email = '';
		public $bcc_email = '';
		public $subject = '';
		public $message = '';
		public $alt_message = '';
		public $reply_email = '';
		public $reply_name = '';
		public $attachments = '';

		public function __construct()
		{
			$this->ci=&get_instance();
			$this->ci->load->library('email');
		}
		
		/**
		 * [initialize with configuration data, some of them are set by default]
		 * @param  array  $config_data [description]
		 * @return [type]              [description]
		 */
		public function initialize($config_data = array(), $app_name, $host)
		{
			$this->m_data = qa_default_emails::$data;
			$this->m_data['subject'] = $app_name . $this->m_data['subject'];
			$this->m_data['message'] = $this->m_data['message']. "<b>".$host."</b><br><br>Thanks";

			$config = qa_default_emails::$email_accounts[$config_data];
			$this->ci->email->initialize($config);
			$this->ci->email->set_newline("\r\n");
		}
			
		public function send($mdata = array())
		{
			$data = array();
			if ($mdata) {
				$data = $mdata;
			} else {
				$data = $this->m_data;
			}

			$from_email	  = isset($data["from_email"])  ? $data["from_email"]  : $this->from_email;
			$from_name	  = isset($data["from_name"])   ? $data["from_name"]   : $this->from_name;              
			$to_email	  = isset($data["to_email"])    ? $data["to_email"]    : $this->to_email;
			$cc_email	  = isset($data["cc_email"])    ? $data["cc_email"]    : $this->cc_email;
			$bcc_email	  = isset($data["bcc_email"])   ? $data["bcc_email"]   : $this->bcc_email;
			$subject      = isset($data["subject"])     ? $data["subject"]     : $this->subject;
			$message      = isset($data["message"])     ? $data["message"]     : $this->message;
			$alt_message  = isset($data["alt_message"]) ? $data["alt_message"] : $this->alt_message;
			$reply_email  = isset($data["reply_email"]) ? $data["reply_email"] : $this->reply_email;
			$reply_name   = isset($data["reply_name"])  ? $data["reply_name"]  : $this->reply_name;
			$attachments  = isset($data["attachments"]) ? $data["attachments"] : $this->attachments;
			
			///** VALIDATION OF INPUT DATA **/	

			// clear attachments and data
			$this->ci->email->clear(TRUE);
			
			$this->ci->email->from($from_email, $from_name);
			//Sets the email address(s) of the recipient(s). Can be a single email, a comma-delimited list or an array:
			$this->ci->email->to($to_email);

			$this->ci->email->subject($subject);
			$this->ci->email->message($message);
			$this->ci->email->set_alt_message($alt_message);
			$this->ci->email->reply_to($reply_email, $reply_name);

			//Sets the CC email address(s). Just like the "to", can be a single email, a comma-delimited list or an array.
			$this->ci->email->cc($cc_email);
			$this->ci->email->bcc($bcc_email);
		
			//Put the file path/name in the first parameter. Note: Use a file path, not a URL. 
			// if(!is_null($attachments))
			// {
			// 	foreach($attachments as $path)
			// 	{
			// 		$this->ci->email->attach($path);
			// 	}
			// }
			if ( ! $this->ci->email->send())
			{
				qa_log::log('error', $this->ci->email->print_debugger());
				qa_log::log("error" , "Error: qa_email->send [email: " .$to_email . " ]");
			}
		}
	}



	class qa_default_emails
	{

		public static $data = array(
										'from_email' => 'QA@questrade.com',
										'from_name'  => 'QA@questrade.com',
										'subject'  	 => ': regression running',
										'message'  	 => "Hi, <br>
		<br>Please don’t use regression assigned user accounts (particularly <b>IQ1003</b>, <b>quest2045</b>, <b>quest2040</b>, <b>quest2050</b>, <b>quest2037</b>) on ",
										'to_email'   => "achaparyan@questrade.com, snersisyan@questrade.com, achuljyan@questrade.com, kisrayelyan@questrade.com, atuniyants@questrade.com, gsmoyan@questrade.com, adavtyan@questrade.com"
									);
		public static $email_accounts = array(
			"noreply" => 
			 array(
					"protocol"  =>	"smtp",  					// 	mail, sendmail, or smtp	The mail sending protocol.
					"smtp_host"	=>	"sq5vqlmsg001",				//  No Default	None	SMTP Server Address.
					"mailtype" =>	"html",						//  text or html	Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don't have any relative links or relative image paths otherwise they will not work.
					"charset"  =>  "utf-8",						//  Character set (utf-8, iso-8859-1, etc.).
					"newline" => "\r\n",						//  \n	"\r\n" or "\n" or "\r"	Newline character. (Use "\r\n" to comply with RFC 822).
				)	
			);
	}

