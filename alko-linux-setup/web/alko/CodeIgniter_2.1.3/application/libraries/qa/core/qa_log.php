<?php

	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class qa_log
	{
		public static $DEBUG = "debug";
		public static $INFO  = "info";
		public static $ERROR = "error";

		/**
		 * [
		 * 	 log -- 
		 *   1)logs into "application/logs" 
		 *   2)and also in Console of browser like console.log in js (depends in which ENVIRONMENT we are now) 
		 * ]
		 * @param  String $type  		[one of the values "error" "debug" "info"]
		 * @param  [Any] $data   		[any data we want to log]
		 * @param  String $label 		[this label will apear in log text]
		 * @return [NO return value]	[no return value]
		 */
		public static function log($type, $data, $label = NULL)
		{
			
			$label = is_null($label) ? $type : $label;
			$log_data = "";	
			$data_type = gettype($data);
			switch($data_type)
			{
				case  "boolean":
				case  "integer":
				case  "double":
				case  "string":
					$log_data = (string)$data;
					break;
				case  "array":
				case  "object":
					$log_data = json_encode($data);
					break;
				case  "NULL": 
					$log_data = "NULL";
					break;
				case  "unknown type":
					$log_data = "UNKNOWN TYPE";
					break;
			}

			switch (ENVIRONMENT) 
			{
				case 'development':
					log_message($type, "label [ ". $label . " ] " . $log_data);
					break;
					
				case 'production':
					log_message($type, "label [ ". $label . " ] " . $log_data);
					break;
			}	
		}
	}


