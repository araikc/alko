/*
ALKO dev DB structure
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Red wine;berried & fresh;<8', 'http://www.alko.fi/en/search/products/?tags=(2848)(2767)(2781)(5500|5492)&priceMax=8.00');
INSERT INTO `categories` VALUES ('2', 'Red wine;berried & fresh;8-9,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2767)(2781)(5500|5492)&priceMin=8.00&priceMax=8.99');
INSERT INTO `categories` VALUES ('3', 'Red wine;berried & fresh;10-12,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2767)(2781)(5500|5492)&priceMin=10.00&priceMax=12.99');
INSERT INTO `categories` VALUES ('4', 'Red wine;berried & fresh;13-14,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2767)(2781)(5500|5492)&priceMin=13.00&priceMax=14.99');
INSERT INTO `categories` VALUES ('5', 'Red wine;berried & fresh;>=15', 'http://www.alko.fi/en/search/products/?tags=(2848)(2767)(2781)(5500|5492)&priceMin=15.00');
INSERT INTO `categories` VALUES ('6', 'Red wine;smooth & fruity;<8', 'http://www.alko.fi/en/search/products/?tags=(2848)(2768)(2781)(5500|5492)&priceMax=8');
INSERT INTO `categories` VALUES ('7', 'Red wine;smooth & fruity;8-9,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2768)(2781)(5500|5492)&priceMin=8.00&priceMax=8.99');
INSERT INTO `categories` VALUES ('8', 'Red wine;smooth & fruity;>=10', 'http://www.alko.fi/en/search/products/?tags=(2848)(2768)(2781)(5500|5492)&priceMin=10.00');
INSERT INTO `categories` VALUES ('9', 'Red wine;luscious & jammy;<7', 'http://www.alko.fi/en/search/products/?tags=(2848)(2769)(2781)(5500|5492)&priceMax=7.00');
INSERT INTO `categories` VALUES ('10', 'Red wine;luscious & jammy;7-7,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2769)(2781)(5500|5492)&priceMin=7.00&priceMax=7.99');
INSERT INTO `categories` VALUES ('11', 'Red wine;luscious & jammy;8-8,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2769)(2781)(5500|5492)&priceMin=8.00&priceMax=8.99');
INSERT INTO `categories` VALUES ('12', 'Red wine;luscious & jammy;9-9,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2769)(2781)(5500|5492)&priceMin=9.00&priceMax=9.99');
INSERT INTO `categories` VALUES ('13', 'Red wine;luscious & jammy;10-10,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2769)(2781)(5500|5492)&priceMin=10.00&priceMax=10.99');
INSERT INTO `categories` VALUES ('14', 'Red wine;luscious & jammy;11-12,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2769)(2781)(5500|5492)&priceMin=11.00&priceMax=12.99');
INSERT INTO `categories` VALUES ('15', 'Red wine;luscious & jammy;13-14,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2769)(2781)(5500|5492)&priceMin=13.00&priceMax=14.99');
INSERT INTO `categories` VALUES ('16', 'Red wine;luscious & jammy;15-19,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2769)(2781)(5500|5492)&priceMin=15.00&priceMax=19.99');
INSERT INTO `categories` VALUES ('17', 'Red wine;luscious & jammy;;>=20', 'http://www.alko.fi/en/search/products/?tags=(2848)(2769)(2781)(5500|5492)&priceMin=20.00');
INSERT INTO `categories` VALUES ('18', 'Red wine;nuanced & developed;<8', 'http://www.alko.fi/en/search/products/?tags=(2848)(4590)(2781)(5500|5492)&priceMax=8.00');
INSERT INTO `categories` VALUES ('19', 'Red wine;nuanced & developed;8-8.99', 'http://www.alko.fi/en/search/products/?tags=(2848)(4590)(2781)(5500|5492)&priceMin=8.00&priceMax=8.99');
INSERT INTO `categories` VALUES ('20', 'Red wine;nuanced & developed;10-12.99', 'http://www.alko.fi/en/search/products/?tags=(2848)(4590)(2781)(5500|5492)&priceMin=10.00&priceMax=12.99');
INSERT INTO `categories` VALUES ('21', 'Red wine;nuanced & developed;13-14,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(4590)(2781)(5500|5492)&priceMin=13.00&priceMax=14.99');
INSERT INTO `categories` VALUES ('22', 'Red wine;nuanced & developed;15-17,49', 'http://www.alko.fi/en/search/products/?tags=(2848)(4590)(2781)(5500|5492)&priceMin=15.00&priceMax=17.49');
INSERT INTO `categories` VALUES ('23', 'Red wine;nuanced & developed;17,50-19,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(4590)(2781)(5500|5492)&priceMin=17.50&priceMax=19.99');
INSERT INTO `categories` VALUES ('24', 'Red wine;nuanced & developed;>=20', 'http://www.alko.fi/en/search/products/?tags=(2848)(4590)(2781)(5500|5492)&priceMin=20.00');
INSERT INTO `categories` VALUES ('25', 'Red wine;robust & powerful;<10', 'http://www.alko.fi/en/search/products/?tags=(2848)(2771)(2781)(5500|5492)&priceMax=10.00');
INSERT INTO `categories` VALUES ('26', 'Red wine;robust & powerful;10-12,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2771)(2781)(5500|5492)&priceMin=10.00&priceMax=12.99');
INSERT INTO `categories` VALUES ('27', 'Red wine;robust & powerful;13-14,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2771)(2781)(5500|5492)&priceMin=13.00&priceMax=14.99');
INSERT INTO `categories` VALUES ('28', 'Red wine;robust & powerful;15-17,49', 'http://www.alko.fi/en/search/products/?tags=(2848)(2771)(2781)(5500|5492)&priceMin=15.00&priceMax=17.49');
INSERT INTO `categories` VALUES ('29', 'Red wine;robust & powerful;17,50-19,99', 'http://www.alko.fi/en/search/products/?tags=(2848)(2771)(2781)(5500|5492)&priceMin=17.50&priceMax=19.99');
INSERT INTO `categories` VALUES ('30', 'Red wine;robust & powerful;>=20', 'http://www.alko.fi/en/search/products/?tags=(2848)(2771)(2781)(5500|5492)&priceMin=20.00');
INSERT INTO `categories` VALUES ('31', 'White wine;smooth & light;<8', 'http://www.alko.fi/en/search/products/?tags=(2850)(2772)(2781)(5500|5492)&priceMax=8.00');
INSERT INTO `categories` VALUES ('32', 'White wine;smooth & light;8-9,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2772)(2781)(5500|5492)&priceMin=8.00&priceMax=9.99');
INSERT INTO `categories` VALUES ('33', 'White wine;smooth & light;>=10', 'http://www.alko.fi/en/search/products/?tags=(2850)(2772)(2781)(5500|5492)&priceMin=10.00');
INSERT INTO `categories` VALUES ('34', 'White wine;mellow & sweet;<8', 'http://www.alko.fi/en/search/products/?section=products&tags=(2850)(2773)(2781)(5500|5492)priceMax=8.00');
INSERT INTO `categories` VALUES ('35', 'White wine;mellow & sweet;8-9,99', 'http://www.alko.fi/en/search/products/?section=products&tags=(2850)(2773)(2781)(5500|5492)&priceMin=8.00&priceMax=9.99');
INSERT INTO `categories` VALUES ('36', 'White wine;mellow & sweet;>=10', 'http://www.alko.fi/en/search/products/?section=products&tags=(2850)(2773)(2781)(5500|5492)&priceMax=10.00');
INSERT INTO `categories` VALUES ('37', 'White wine;crisp & fruity;<7', 'http://www.alko.fi/en/search/products/?tags=(2850)(2774)(2781)(5500|5492)&priceMax=7.00');
INSERT INTO `categories` VALUES ('38', 'White wine;crisp & fruity;7-7,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2774)(2781)(5500|5492)&priceMin=7.00&priceMax=7.99');
INSERT INTO `categories` VALUES ('39', 'White wine;crisp & fruity;8-8,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2774)(2781)(5500|5492)&priceMin=8.00&priceMax=8.99');
INSERT INTO `categories` VALUES ('40', 'White wine;crisp & fruity;9-9,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2774)(2781)(5500|5492)&priceMin=9.00&priceMax=9.99');
INSERT INTO `categories` VALUES ('41', 'White wine;crisp & fruity;10-12,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2774)(2781)(5500|5492)&priceMin=10.00&priceMax=12.99');
INSERT INTO `categories` VALUES ('42', 'White wine;crisp & fruity;13-19,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2774)(2781)(5500|5492)&priceMin=13.00&priceMax=19.99');
INSERT INTO `categories` VALUES ('43', 'White wine;crisp & fruity;>=20', 'http://www.alko.fi/en/search/products/?tags=(2850)(2774)(2781)(5500|5492)&priceMin=20.00');
INSERT INTO `categories` VALUES ('44', 'White wine;nuanced & structured;<8', 'http://www.alko.fi/en/search/products/?tags=(2850)(2775)(2781)(5500|5492)&priceMax=8.00');
INSERT INTO `categories` VALUES ('45', 'White wine;nuanced & structured;8-8,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2775)(2781)(5500|5492)&priceMin=8.00&priceMax=8.99');
INSERT INTO `categories` VALUES ('46', 'White wine;nuanced & structured;9-9,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2775)(2781)(5500|5492)&priceMin=9.00&priceMax=9.99');
INSERT INTO `categories` VALUES ('47', 'White wine;nuanced & structured;10-12,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2775)(2781)(5500|5492)&priceMin=10.00&priceMax=12.99');
INSERT INTO `categories` VALUES ('48', 'White wine;nuanced & structured;13-14,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2775)(2781)(5500|5492)&priceMin=13.00&priceMax=14.99');
INSERT INTO `categories` VALUES ('49', 'White wine;nuanced & structured;15-17,49', 'http://www.alko.fi/en/search/products/?tags=(2850)(2775)(2781)(5500|5492)&priceMin=15.00&priceMax=17.49');
INSERT INTO `categories` VALUES ('50', 'White wine;nuanced & structured;17,50-19,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2775)(2781)(5500|5492)&priceMin=17.50&priceMax=19.99');
INSERT INTO `categories` VALUES ('51', 'White wine;nuanced & structured;>=20', 'http://www.alko.fi/en/search/products/?tags=(2850)(2775)(2781)(5500|5492)&priceMin=20.00');
INSERT INTO `categories` VALUES ('52', 'White wine;generous & toasty;<10', 'http://www.alko.fi/en/search/products/?tags=(2850)(2776)(2781)(5500|5492)&priceMax=10.00');
INSERT INTO `categories` VALUES ('53', 'White wine;generous & toasty;10-14,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2776)(2781)(5500|5492)&priceMin=10.00&priceMax=14.99');
INSERT INTO `categories` VALUES ('54', 'White wine;generous & toasty;15-19,99', 'http://www.alko.fi/en/search/products/?tags=(2850)(2776)(2781)(5500|5492)&priceMin=15.00&priceMax=19.99');
INSERT INTO `categories` VALUES ('55', 'White wine;generous & toasty;>=20', 'http://www.alko.fi/en/search/products/?tags=(2850)(2776)(2781)(5500|5492)&priceMin=20.00');
INSERT INTO `categories` VALUES ('56', 'Sparkling wine; extra Brut<8', 'http://www.alko.fi/en/search/products/?tags=(7014)(4267)(2781)(5500|5492)&priceMax=8');
INSERT INTO `categories` VALUES ('57', 'Sparkling wine; Extra Brut8-9,99', 'http://www.alko.fi/en/search/products/?tags=(7014)(4267)(2781)(5500|5492)&priceMin=8.00&priceMax=9.99');
INSERT INTO `categories` VALUES ('58', 'Sparkling wine;Extra Brut10-14,99', 'http://www.alko.fi/en/search/products/?tags=(7014)(4267)(2781)(5500|5492)&priceMin=10.00&priceMax=14.99');
INSERT INTO `categories` VALUES ('59', 'Sparkling wine; Extra Brut>=15', 'http://www.alko.fi/en/search/products/?tags=(7014)(4267)(2781)(5500|5492)&priceMin=15.00');
INSERT INTO `categories` VALUES ('60', 'Sparkling wine; Brut <8', 'http://www.alko.fi/en/search/products/?tags=(7014)(4268)2781)(5500|5492)&priceMax=8.00');
INSERT INTO `categories` VALUES ('61', 'Sparkling wine; Brut 8-9,99', 'http://www.alko.fi/en/search/products/?tags=(7014)(4268)2781)(5500|5492)&priceMin=8.00&priceMax=9.99');
INSERT INTO `categories` VALUES ('62', 'Sparkling wine; Brut 10-', 'http://www.alko.fi/en/search/products/?tags=(7014)(4268)2781)(5500|5492)&priceMin=10.00');
INSERT INTO `categories` VALUES ('63', 'Sparkling wine; Sweet<10', 'http://www.alko.fi/en/search/products/?tags=(7014)(9113)(4272)(5500|5492)&priceMax=10.00');
INSERT INTO `categories` VALUES ('64', 'Sparkling wine; Sweet>=10', 'http://www.alko.fi/en/search/products/?tags=(7014)(9113)(4272)(5500|5492)&priceMin=10.00');
INSERT INTO `categories` VALUES ('65', 'Sparkling wine;Sparkling wine;Demi-sec;<10', 'http://www.alko.fi/en/search/products/?tags=(7014)(2781)(9113)(4270)5500|5492)&priceMax=10.00');
INSERT INTO `categories` VALUES ('66', 'Sparkling wine;Sparkling wine;Demi-sec;>=10', 'http://www.alko.fi/en/search/products/?tags=(7014)(2781)(9113)(4270)(5500|5492)&priceMin=10.00');
INSERT INTO `categories` VALUES ('67', 'Sparkling wine;Champagne;Extra brut;<29,99', 'http://www.alko.fi/en/search/products/?tags=(7014)(9110)(4267)(2781)(5500|5492)&priceMin=15.00&priceMax=29.99');
INSERT INTO `categories` VALUES ('68', 'Sparkling wine;Champagne;Extra brut;30-39,99', 'http://www.alko.fi/en/search/products/?tags=(7014)(9110)(4267)(2781)(5500|5492)&priceMin=30.00&priceMax=39.99');
INSERT INTO `categories` VALUES ('69', 'Sparkling wine;Champagne;Extra brut;40-49,99', 'http://www.alko.fi/en/search/products/?tags=(7014)(9110)(4267)(2781)(5500|5492)&priceMin=40.00&priceMax=49.99');
INSERT INTO `categories` VALUES ('70', 'Sparkling wine;Champagne;Extra brut;50-99,99', 'http://www.alko.fi/en/search/products/?tags=(7014)(9110)(4267)(2781)(5500|5492)&priceMin=50.00&priceMax=99.99');
INSERT INTO `categories` VALUES ('71', 'Sparkling wine;Aromatised sparkling wine', 'http://www.alko.fi/en/search/products/?tags=(2781)(7014)(9111)(5500|5492)');
INSERT INTO `categories` VALUES ('72', 'Sparkling wine;Fruit sparkling wine', 'http://www.alko.fi/en/search/products/?tags=(2781)(7014)(9112)(5500|5492)');
INSERT INTO `categories` VALUES ('73', 'Rose wine;dry', 'http://www.alko.fi/en/search/products/?tags=(2781)(2849)(4268)(5500|5492)');
INSERT INTO `categories` VALUES ('74', 'Rose wine;medium dry', 'http://www.alko.fi/en/search/products/?tags=(2781)(2849)(4270)(5500|5492)');
INSERT INTO `categories` VALUES ('75', 'Rose wine;medium sweet', 'http://www.alko.fi/en/search/products/?tags=(2781)(2849)(4271)(5500|5492)');
INSERT INTO `categories` VALUES ('76', 'Rose wine;sweet', 'http://www.alko.fi/en/search/products/?tags=(2781)(2849)(4272)(5500|5492)&priceMin=6.00&priceMax=20.00');
INSERT INTO `categories` VALUES ('77', 'Bag in box ;Red wine;<20', 'http://www.alko.fi/en/search/products/?tags=(7028)(7029)(2781)&priceMin=10.00&priceMax=20.00');
INSERT INTO `categories` VALUES ('78', 'Bag in Box;Red wine;20-24,99', 'http://www.alko.fi/en/search/products/?tags=(7028)(7029)(2781)&priceMin=20.00&priceMax=24.99');
INSERT INTO `categories` VALUES ('79', 'Bag in Box Red wine;25-26,99', 'http://www.alko.fi/en/search/products/?tags=(7028)(7029)(2781)&priceMin=25.00&priceMax=26.99');
INSERT INTO `categories` VALUES ('81', 'Bag in Box;Red wine;27-29,99', 'http://www.alko.fi/en/search/products/?tags=(7028)(7029)(2781)&priceMin=27.00&priceMax=29.99');
INSERT INTO `categories` VALUES ('82', 'Bag in Box;Red wine;>=30', 'http://www.alko.fi/en/search/products/?tags=(7028)(7029)(2781)&priceMin=30.00');
INSERT INTO `categories` VALUES ('83', 'Bag in Box;White wine;<20', 'http://www.alko.fi/en/search/products/?tags=(7028)(2781)(7031)&priceMin=10.00&priceMax=20.00');
INSERT INTO `categories` VALUES ('84', 'Bag in Box;White wine;20-24,99', 'http://www.alko.fi/en/search/products/?tags=(7028)(2781)(7031)&priceMin=20.00&priceMax=24.99');
INSERT INTO `categories` VALUES ('85', 'Bag in Box;White wine;25-26,99', 'http://www.alko.fi/en/search/products/?tags=(7028)(2781)(7031)&priceMin=25.00&priceMax=26.99');
INSERT INTO `categories` VALUES ('86', 'Bag in Box;White wine;27-29,99', 'http://www.alko.fi/en/search/products/?tags=(7028)(2781)(7031)&priceMin=27.00&priceMax=29.99');
INSERT INTO `categories` VALUES ('87', 'Bag in Box;White wine;>=30', 'http://www.alko.fi/en/search/products/?tags=(7028)(2781)(7031)&priceMin=30.00');

-- ----------------------------
-- Table structure for months
-- ----------------------------
DROP TABLE IF EXISTS `months`;
CREATE TABLE `months` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `dumped` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `stores_count` int(11) DEFAULT NULL,
  `all_items_count` int(11) DEFAULT NULL,
  `month_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14580 DEFAULT CHARSET=utf8;

