import os
import re
import MySQLdb
from alko.adb import adb

def main():
    run_crawler()
    print "Successfully run"

def run_crawler():
    print 'Running crawler...'
    if os.path.isfile('log.txt'):
        os.remove('log.txt')
    # run scrapy
    os.system("scrapy crawl alko > log.txt 2>&1")
    check_results()

def check_results():
    print 'Checking results...'
    logdata=''
    with open ("log.txt", "r") as myfile:
        logdata=myfile.read().replace('\n', '')
    m = re.match('.*ERROR: Spider error processing.*', logdata)
    if m:
            print "Error encountered: re-runnig again. Please wait."
            db = adb.Model()
            m_id = db.get_last_id('months')
            db.delete_prods_with_month_id(m_id)
            db.delete_month(m_id)
            run_crawler()
    m = re.match('.*Data for this months already get.*', logdata)
    if m:
            print "Data for this month already get"
            exit(1)

# main entry
main()

