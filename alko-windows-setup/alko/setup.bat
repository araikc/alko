@set PATH=%PATH%;"c:\Program Files (x86)\Git\bin"
@boot2docker.exe start
@set DOCKER_HOST=tcp://192.168.59.103:2376
@set DOCKER_CERT_PATH=C:\Users\%username%\.boot2docker\certs\boot2docker-vm
@set DOCKER_TLS_VERIFY=1
@echo "Removing docker all containers"
@docker ps -q -a > queue
@for /F "tokens=*" %%l in (queue) do @docker stop %%l
@for /F "tokens=*" %%l in (queue) do @docker rm %%l
@echo "Creating docker containers"
@docker run -d -p 8050:8050 araikc/alko python -m splash.server --port=8050
@docker run -d -p 80:80 -v /c/Users/%username%/workspace/alko/data/source/alko:/var/www/betchili.com araikc/betchili /usr/sbin/apache2ctl -D FOREGROUND
@docker run -d -p 3306:3306  -v /c/Users/%username%/workspace/alko/data/mysql:/var/lib/mysql  araikc/mysql_alko
