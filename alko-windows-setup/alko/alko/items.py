# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ALKOItem(scrapy.Item):
    # define the fields for your item here like:
    Name = scrapy.Field()
    Price = scrapy.Field()
    Category = scrapy.Field()
    Country = scrapy.Field()
    #Link = scrapy.Field()
    StoresCount = scrapy.Field()
    AllItemsCount = scrapy.Field()

