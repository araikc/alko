import MySQLdb
import os

USER='alko'
PASSWD='a1l2k3o4'
DB_NAME='alko_db'
HOST='127.0.0.1'
PORT=3307

class Model():
    def __init__(self):
        self.months = {1: 'January', 2: 'February', 3: 'March', 4: 'April', 5: 'May',
                       6: 'June', 7: 'July', 8: 'August', 9: 'September', 
                       10: 'October', 11: 'November', 12: 'December'}
        self.conn = MySQLdb.connect(user=USER, passwd=PASSWD, db=DB_NAME, host=HOST, port=PORT, charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def get_categories(self):
        try:
          cats = []
          self.cursor.execute("select * from categories")
          for (i, name, url) in self.cursor:
            cats.append({'name': name, 'id': i, 'url': url}) 
          return cats
        except MySQLdb.Error, e:
          print "Error %d: %s" % (e.args[0], e.args[1])

    def get_months(self):
        try:
          months = []
          self.cursor.execute("select * from months")
          for (i, name, dumped) in self.cursor:
            months.append({'name': name, 'id': i, 'dumped': dumped}) 
          return months
        except MySQLdb.Error, e:
          print "Error %d: %s" % (e.args[0], e.args[1])

    def add_month(self, month):
        try:
          self.cursor.execute(u"INSERT INTO months(name, dumped) VALUES ('%s', 1)" % (month))
          self.conn.commit()
        except MySQLdb.Error, e:
          print "Error %d: %s" % (e.args[0], e.args[1])

    def add_product(self, data):
        try:
          sql = u'INSERT INTO products (name, category_id, country, price, stores_count, all_items_count, month_id) VALUES ("%s", %s, "%s", %s, %s, %s, %s)' % (
              data['name'],
              data['category_id'],
              data['country'],
              data['price'],
              data['stores_count'],
              data['all_items_count'],
              data['month_id']
              )
          self.cursor.execute(sql)
          self.conn.commit()
        except MySQLdb.Error, e:
          print "Error %d: %s" % (e.args[0], e.args[1])

    def get_last_id(self, table):
        try:
          self.cursor.execute("select id from %s order by id DESC LIMIT 1" % table)
          sid=None
          for row in self.cursor:
            sid=row[0]
          return sid
        except MySQLdb.Error, e:
          print "Error %d: %s" % (e.args[0], e.args[1])

    def close(self):
      self.cursor.close()
      self.conn.close()

    # Get current month
    def get_current_month(self):
        import datetime
        m = datetime.datetime.now().month
        mname = self.months[m]
        y = datetime.datetime.now().year
        return "%s-%s" % (mname, str(y))

    # delete products with spec id
    def delete_prods_with_month_id(self, m_id):
        try:
          self.cursor.execute("delete from products where month_id=%s" % m_id)
          self.conn.commit()
        except MySQLdb.Error, e:
          print "Error %d: %s" % (e.args[0], e.args[1])

    # delete month with spec id
    def delete_month(self, m_id):
        try:
          self.cursor.execute("delete from months where id=%s" % m_id)
          self.conn.commit()
        except MySQLdb.Error, e:
          print "Error %d: %s" % (e.args[0], e.args[1])

